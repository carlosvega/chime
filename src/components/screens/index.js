// import split from '../../lib/split-point';
//
// export default {
// 	error: split(() => import('./error')),
// 	home: split(() => import('./home')),
// 	login: split(() => import('./login')),
// 	notifications: split(() => import('./notifications')),
// 	profile: split(() => import('./profile')),
// 	public: split(() => import('./public')),
// 	search: split(() => import('./search'))
// };


const requireAll = ctx => ctx.keys().filter( x => /\/[^.]+$/.test(x) ).reduce( (acc, m) => {
	if (m!=='./index') {
		let c = ctx(m);
		acc[m.replace(/(^\.?\/|(\/index)?\.js$)/g, '')] = c.default || c;
	}
	return acc;
}, {});

export default requireAll(require.context('.', true));
