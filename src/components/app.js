import { h, Component } from 'preact';
import { Router, route } from 'preact-router';
import Provider from '../lib/provider';
import Header from './header';
import ModalViewer from './modal-viewer';
import screens from './screens';
import createStore from '../store';
import { connect, select } from '../lib/store';
import chimeClient from '../lib/chime';
import config from '../config';

let store = createStore({
	config
});

let chime = chimeClient({
	config,
	store
});

window.chime = chime;

@connect(select(['loggedin', 'authfailed', 'username', 'error']))
class AppRoot extends Component {
	handleUrlChange({ url }) {
		let prev = store.getState().url || '/';
		if (url===prev) return;
		store.setState({ url });
		if (typeof ga==='function') {
			ga('send', 'pageview', url);
		}
	}

	componentDidUpdate() {
		if (this.props.authfailed && this.props.url!=='/login') {
			route('/login');
		}
	}

	render({ loggedin, username, error }) {
		return (
			<div id="app">
				<Header />

				<ModalViewer />

				<main>
					<Router onChange={this.handleUrlChange}>
						{ loggedin ? ([
							<screens.home path="/" />,
							<screens.public path="/public" />,
							<screens.notifications path="/notifications" />,
							<screens.profile path="/profile" user={username} />
						]) : ([
							<screens.public path="/" />,
							<screens.login path="/login" mode="login" />,
							<screens.login path="/register" mode="register" />
						]) }
						<screens.profile path="/profile/:user" />
						<screens.profile path="/profile/:user/:section" />
						<screens.search path="/search" />
						<screens.error default />
					</Router>
				</main>

				{/* { error && (
					<Toast message={error.message || error} time={2} />
				) } */}
			</div>
		);
	}
}


export default () => (
	<Provider store={store} chime={chime}>
		<AppRoot />
	</Provider>
);
