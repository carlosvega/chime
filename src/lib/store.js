import { h, Component } from 'preact';
import shallowEqual from './shallow-equal';


export default (state={}) => {
	let listeners = [];

	return {
		setState(update) {
			state = { ...state, ...update };
			listeners.forEach( f => f(state) );
		},
		subscribe(f) {
			listeners.push(f);
		},
		unsubscribe(f) {
			let i = listeners.indexOf(f);
			listeners.splice(i, !!~i);
		},
		getState() {
			return state;
		}
	};
};


export class Provider extends Component {
	getChildContext() {
		let { children, ...context } = this.props;
		return context;
	}
	render({ children }) {
		return children[0];
	}
}


export function connect(mapToProps) {
	if (typeof mapToProps!=='function') mapToProps = select(mapToProps);
	return Child => class Wrapper extends Component {
		state = this.getProps();
		update = () => {
			let mapped = this.getProps();
			if (!shallowEqual(mapped, this.state)) {
				this.setState(mapped);
			}
		};
		getProps() {
			let state = this.context.store && this.context.store.getState() || {};
			return mapToProps(state);
		}
		componentWillMount() {
			this.context.store.subscribe(this.update);
		}
		componentWillUnmount() {
			this.context.store.unsubscribe(this.update);
		}
		render(props, state, context) {
			return <Child store={context.store} {...props} {...state} />;
		}
	};
}


export function select(properties) {
	if (typeof properties==='string') properties = properties.split(',');
	return state => {
		let selected = {};
		for (let i=0; i<properties.length; i++) {
			selected[properties[i]] = state[properties[i]];
		}
		return selected;
	};
}
