import { h } from 'preact';
import ImageViewer from '../components/image-viewer';


/** Cached parsed post bodies (as JSX Children), keyed on `${resource.type}-${resource.id}` */
const CACHE = {};


/** Parses a Post (body) into an Array of JSX children (Strings and Elements) representing the rich post body.
 *	@param {Object} post
 *	@return {Array<String,VNode>} an Array of JSX children
 */
export default post => {
	let resourceType = typeof post.post_id==='undefined' ? 'post' : 'comment',
		key = resourceType + '-' + post.id;

	// return from cache if possible
	if (CACHE.hasOwnProperty(key)) return CACHE[key];

	let { body, entities } = post,
		media = [],
		ranges = [];

	for (let type in entities) if (entities.hasOwnProperty(type)) {
		for (let i=0; i<entities[type].length; i++) {
			ranges.push({
				...entities[type][i],
				type: type.replace(/s$/, '')
			});
		}
	}

	if (ranges.length) {
		ranges.sort( (a, b) => a.indices[0] - b.indices[0] );

		let out = [],
			lastIndex = 0;
		for (let i=0; i<ranges.length; i++) {
			let range = ranges[i];
			out.push(utf16Substring(body, lastIndex, range.indices[0]));
			let text = utf16Substring(body, range.indices[0], range.indices[1]);
			out.push(ENTITIES[range.type](range, text, media));
			lastIndex = range.indices[1];
		}
		out.push(utf16Substring(body, lastIndex));

		body = out.filter(Boolean);

		if (media.length) {
			body.push(
				<div class="post-media tile">
					{media}
				</div>
			);
		}
	}

	return CACHE[key] = body;
};



/** Maps entity types to components to inject in their place. */
const ENTITIES = {
	hashtag: (entity, text) => (
		<a href={`/search?q=${encodeURIComponent('#'+entity.hashtag)}`}>{text}</a>
	),

	url(entity, text, media) {
		let img = getImageUrl(entity.url);
		if (img) {
			media.push( <Media type="image" src={img} url={entity.url} /> );
			return null;
		}
		return <a href={entity.url} target="_blank" rel="nofollow noopener noreferrer">{text}</a>;
	},

	user_mention: (entity, text) => (
		<a href={`/profile/${encodeURIComponent(entity.screen_name)}`}>{text}</a>
	)
};



const IS_IMAGE = /\.(gif|png|jpe?g)$/;
const IMGUR = /^(?:https?:)?\/\/(?:i\.)?imgur\.com\/(?:[a-z]\/)?([^./?]+)/i;
const TO_HTTPS = /^http:(\/\/(?:i\.)?giphy\.com\/.*)$/i;
function getImageUrl(url) {
	let m = url.match(IMGUR);
	if (m && m[1]) return `https://i.imgur.com/${m[1]}.png`;
	if (IS_IMAGE.test(url)) {
		let p = url.match(TO_HTTPS);
		return p ? p[1] : url;
	}
}



/** Thumbnail component that triggers a modal image viewer. */
const Media = ({ type, src, url }) => {
	let thumb = src,
		m = src.match(/^((?:https?:)\/\/i.imgur.com\/.*?)[sbtmlh]?(\.[a-z]+(?:\?.*)?)$/);
	if (m) thumb = m[1] + 'h' + m[2];

	return (
		<div class="tile post-media-item is-image">
			<ImageViewer src={src} thumb={thumb}>
				<img src={thumb} style={{ maxHeight: '250px' }} />
			</ImageViewer>
		</div>
	);
};



/** Like ''.substring, but treats unicode surrogate pairs as a single character width */
function utf16Substring(str, start, end) {
	let out = '', i = 0, index = 0;
	for (; i<str.length; i++, index++) {
		let char = str.charAt(i),
			code = str.charCodeAt(i);
		if (code>=0xd800 && code<=0xdfff) {
			char += str.charAt(++i);
		}
		if (index>=start && (end==null || index<end)) {
			out += char;
		}
	}
	return out;
}
