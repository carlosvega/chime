import { h, Component } from 'preact';

export default (load, fallback) => {
	let cached;

	return class LazyWrapper extends Component {
		state = {
			child: cached
		};

		componentWillMount() {
			if (!this.state.child) {
				let callback = child => {
					cached = child;
					this.setState({ child });
				};
				let p = load(callback);
				if (p && p.then) p.then(callback);
			}
		}

		render(props, { child }) {
			return child ? h(child.default || child, props) : fallback || <noscript />;
		}
	};
};
