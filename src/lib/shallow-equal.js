export default function shallowEqual(a, b) {
	for (let i in a) if (a[i]!==b[i]) return false;
	for (let i in b) if (!(i in a)) return false;
	return true;
}
