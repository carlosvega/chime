import { h, Component } from 'preact';
import distanceInWordsToNow from 'date-fns/distance_in_words_to_now';

const TIMES = [];

setInterval( () => {
	let update = { now: Date.now() };
	TIMES.forEach( time => time.setState(update) );
}, 60 * 1000);

export default class RelativeTime extends Component {
	componentDidMount() {
		TIMES.push(this);
	}
	componentWillUnmount() {
		let index = TIMES.indexOf(this);
		if (~index) TIMES.splice(index, 1);
	}
	render({ time }) {
		let local = new Date(time.replace(' ','T')+'.000Z'),
			relative = distanceInWordsToNow(local, { addSuffix: true });
		return <time title={local.toLocaleString()}>{relative}</time>;
	}
}
