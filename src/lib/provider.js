import { Component } from 'preact';

export default class Provider extends Component {
	getChildContext() {
		let { children, ...context } = this.props;
		return context;
	}

	render({ children }) {
		return children[0];
	}
}
