import createStore from './lib/store';
import { debounce } from 'decko';

export default ({ id, config }) => {
	let store = createStore();

	id = id || config.storeId || 'chimedata';

	// populate innitially from localStorage
	try {
		store.setState(JSON.parse(localStorage.getItem(id)) || {});
	} catch (e) {}

	// save to localStorage after writes
	store.subscribe(debounce(500, data => {
		try {
			localStorage.setItem(id, JSON.stringify(data));
		} catch (err) { console.error(err); }
	}));

	return store;
};
